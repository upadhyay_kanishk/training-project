import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LifecycleComponent}from './lifecycle/lifecycle.component';
import {CounterComponent}from './counter/counter.component'
import { DirectiveComponent } from './directive/directive.component';
import { PipeComponent } from './pipe/pipe.component';
import { ContentProjectionComponent } from './content-projection/content-projection.component';
const routes: Routes = [
  {path:'',redirectTo:'lifecycle-hooks',pathMatch:'full'},
  {path:'lifecycle-hooks',component:LifecycleComponent},
  {path:'service',component:CounterComponent},
  {path:'directive',component:DirectiveComponent},
  {path:'pipe',component:PipeComponent},
  {path:'content-projection',component:ContentProjectionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
