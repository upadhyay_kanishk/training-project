import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'custom'
})
export class CustomPipe implements PipeTransform {

  transform(value: string, gender:string){
    console.log(gender)
    if(gender==='Male'){
      return  'Mr.'+value
    }
    else{
      return 'Miss.'+ value
    }
  }

}
