import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LifecycleComponent } from './lifecycle/lifecycle.component';
import { DirectiveComponent } from './directive/directive.component';
import { PipeComponent } from './pipe/pipe.component';
import { CounterComponent } from './counter/counter.component';
import {Router, RouterModule}from '@angular/router'
import {MatButtonModule} from '@angular/material/button';
import { CustomDirective } from './directive/custom.directive';
import { CustomPipe } from './pipe/custom.pipe';
import { ContentProjectionComponent } from './content-projection/content-projection.component';
import { ChildComponent } from './child/child.component';
import { LifecycleChildComponent } from './lifecycle-child/lifecycle-child.component';


@NgModule({
  declarations: [
    AppComponent,
    LifecycleComponent,
    DirectiveComponent,
    PipeComponent,
    CounterComponent,
    CustomDirective,
    CustomPipe,
    ContentProjectionComponent,
    ChildComponent,
    LifecycleChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
