import { Component, ViewChild } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myapp';
  name='Kanishk Upadhyay'
  data=[
    {name:"kanishk",age:19,language:'Javascript'},
    {name:'satyam',age:19,language:'C++'}
  ]
}
