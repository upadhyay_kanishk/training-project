import { Component, OnInit } from '@angular/core';
import { DesignService } from '../service/design.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {
value:number=0
  constructor(private designService:DesignService) { }

  ngOnInit(): void {
    this.designService.number.subscribe(data=>this.value=data)
  }
  increment(){
    if(this.value>=0){
      this.value++
    }
  }
  decrement(){
    if(this.value>0){
      this.value--
    }
  }
}
